pipeline {
    agent{
        labels 'group1'
	}

    stages {
        stage('Install Docker') {
            steps {
                script {
                    // Install Docker on the Jenkins agent machine
                    sh 'sudo apt-get update && sudo apt-get install -y docker.io'
                }
            }
        }

        stage('Print Docker Version') {
            steps {
                script {
                    // Print the Docker version
                    sh 'docker --version'
                }
            }
        }
    }
}

